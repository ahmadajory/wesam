import 'package:app/src/routes/Router.gr.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:app/util/Colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(374.8372915849355, 796.1821730517055),
      allowFontScaling: false,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primaryColor: AppColors.primary,
        ),
        debugShowCheckedModeBanner: false,
        builder: ExtendedNavigator.builder<CustomRouter>(
          router: CustomRouter(),
        ),
      ),
    );
  }
}
