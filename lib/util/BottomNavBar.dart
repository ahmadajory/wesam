import 'package:flutter/material.dart';
import 'package:app/util/Colors.dart';
import 'package:app/util/TextStyles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomBottomNavigatorBar extends StatefulWidget {
  final int defaultSelectedIndex;
  final List<Widget> icons;
  final List<String> texts;
  final Function(int) onChange;

  CustomBottomNavigatorBar({this.defaultSelectedIndex = 0,
    @required this.icons,
    @required this.texts,
    @required this.onChange});

  @override
  _CustomBottomNavigatorBarState createState() => _CustomBottomNavigatorBarState();
}

class _CustomBottomNavigatorBarState extends State<CustomBottomNavigatorBar> {
  int _selectedIndex;
  List<Widget> _icons;
  List<String> _texts;

  @override
  void initState() {
    super.initState();
    _selectedIndex = widget.defaultSelectedIndex;
    _icons = widget.icons;
    _texts = widget.texts;
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _navBarItems = [];
    for(var i = 0; i < _icons.length; i++){
      _navBarItems.add(buildNavBarItem(_icons[i], _texts[i], i));
    }
    return Row(
      children: _navBarItems,
    );
  }


  Widget buildNavBarItem(Widget icon, String text, int index)
  {
    bool _isActive = index == _selectedIndex;
    return GestureDetector(
      onTap: (){
        widget.onChange(index);
        setState(() {
          _selectedIndex = index;
        });
      },
      child: Container(
        width: (MediaQuery.of(context).size.width - NarBarConfigs.WIDTH_PADDING * 2) / _icons.length,
        height: NarBarConfigs.HEIGHT,
        padding:  EdgeInsets.only(left: 5.w, right: 5.w,top: 14.h, bottom: 14.h),
        child: Container(
          height: NarBarConfigs.HEIGHT / 2,
          decoration: _isActive ? BoxDecoration(
            color: AppColors.primary,
            borderRadius: BorderRadius.circular(NarBarConfigs.HEIGHT / 3),
          ) : BoxDecoration(),
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                icon,
                SizedBox(width: 8.w,),
                _isActive ? Text(text,style: TextStyles.StyleBold12(), ) : Text("")
              ],),
          ),
        ),
      ),
    );
  }
}

class NarBarConfigs{
  static final double HEIGHT = 60.h;
  static final double WIDTH_PADDING= 20.w;
  static final double BOTTOM_PADDING= 10.h;
}
