import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:app/util/Colors.dart';
import 'package:app/util/TextStyles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class NormalButton extends StatelessWidget {
  final GestureTapCallback onPressed;
  final String text;
  // final EdgeInsets padding ;
  final double buttonHeight;
  final double verticalPadding;
  final double horizontalPadding;
  final Color fillColor;
  final Color splashColor;
  NormalButton(
      {@required this.onPressed,
      this.text,
      this.buttonHeight = 39,
      this.verticalPadding = 8,
      this.horizontalPadding = 50,
      this.fillColor = AppColors.primary,
      this.splashColor = AppColors.primarySplash});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: buttonHeight.h,
      child: RawMaterialButton(
        onPressed: onPressed,
        fillColor: fillColor,
        splashColor: splashColor,
        shape: StadiumBorder(),
        child: Padding(
          padding: EdgeInsets.symmetric(
              vertical: verticalPadding.h, horizontal: horizontalPadding.w),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[Text(text, style: TextStyles.buttonStyle(color:  AppColors.LightBlack))],
          ),
        ),
      ),
    );
  }
}

class NormalButtonWithIcon extends StatelessWidget {
  final GestureTapCallback onPressed;
  final String text;
  final TextStyle textStyle;
  // final EdgeInsets padding;
  final Color fillColor;
  final Color splashColor;
  final Image image;
  NormalButtonWithIcon(
      {@required this.onPressed,
      this.text,
      @required this.textStyle,
      this.image,
      // this.padding = const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
      this.fillColor = Colors.white,
      this.splashColor = Colors.grey});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: onPressed,
      color: fillColor,
      splashColor: splashColor,
      shape: StadiumBorder(),
      child: Padding(
        padding: EdgeInsets.only(bottom: 10.h, top: 10.h),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[image, Text(text, style: textStyle)],
        ),
      ),
    );
  }
}
