import 'package:flutter/material.dart';
import 'package:app/util/Colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TextStyles {
  static TextStyle headStyleBold({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 30.sp, fontWeight: FontWeight.bold);
  static TextStyle headStyleLight({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 30.sp, fontWeight: FontWeight.w200);

  static TextStyle subTitleStyle({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 16.sp, fontWeight: FontWeight.w500);
  static TextStyle buttonStyle({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 14.sp, fontWeight: FontWeight.w600);
  static TextStyle smallText({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 14.sp, fontWeight: FontWeight.normal);
  static TextStyle smallTextMedium({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 14.sp, fontWeight: FontWeight.w500);
  static TextStyle smallTextBold({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 14.sp, fontWeight: FontWeight.bold);

  static TextStyle StyleBold18({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 18.sp, fontWeight: FontWeight.bold);
  static TextStyle StyleLight18({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 18.sp, fontWeight: FontWeight.w400);

  static TextStyle StyleNormal12({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 12.sp, height: 1.8.h);
  static TextStyle StyleBold12({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 12.sp, fontWeight: FontWeight.bold);
  static TextStyle StyleMeduim12({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 12.sp, fontWeight: FontWeight.w600);
  static TextStyle Style12({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 12.sp);

  static TextStyle StyleBold22({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 22.sp, fontWeight: FontWeight.bold);
  static TextStyle StyleMedium22({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 22.sp, fontWeight: FontWeight.w600);
  static TextStyle StyleMedium20({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 20.sp, fontWeight: FontWeight.w600);

  static TextStyle StyleBold14({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 14.sp, fontWeight: FontWeight.bold);
  static TextStyle StyleMedium14({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 14.sp, fontWeight: FontWeight.w600);
  static TextStyle StyleNormal14({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 14.sp);

  static TextStyle StyleBold10({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 10.sp, fontWeight: FontWeight.bold);
  static TextStyle StyleNormal10({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 10.sp);

  static TextStyle StyleBold16({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 16.sp, fontWeight: FontWeight.bold);
  static TextStyle StyleNormal16({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 16.sp);

  static TextStyle StyleBold28({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 28.sp, fontWeight: FontWeight.w600);

  static TextStyle StyleBold8({color = AppColors.black}) =>
      TextStyle(color: color, fontSize: 8.sp, fontWeight: FontWeight.w600);
}
