import 'package:flutter/material.dart';
class AppColors{
  static const primary = Color(0xffFFE600);
  static const primarySplash = Colors.amber;
  static const white = Color(0xffffffff);
  static const black = Color(0xff141414);
  static const LightBlack = Color(0xff171725);
  static const darkRed = Color(0xffFF3B3B);
  static const facebookButtonColor = Color(0xFF475993);
}