import 'package:flutter/material.dart';
import 'package:app/util/Assets.dart';
import 'package:app/util/TextStyles.dart';
import 'package:app/util/Colors.dart';
import 'package:app/src/models/MatchTypes.dart';
class MatchItem extends StatelessWidget {
  final MatchesType viewType;
  MatchItemPlace place;
  MatchItem({this.viewType, this.place = MatchItemPlace.LIST});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 20, left: 10, right: 10),
      height: 157,
      decoration: place == MatchItemPlace.LIST ? BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.1),
              spreadRadius: 8,
              blurRadius: 8,
              offset: Offset(0, 3), // changes position of shadow
            )
          ]
      ) : BoxDecoration(),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 10),
        child: Row(

          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            TeamColumn(viewType: viewType),
            ResultColumn(viewType: viewType),
            TeamColumn(viewType: viewType),
          ],
        ),
      ),
    );
  }
}

class TeamColumn extends StatelessWidget {
  final MatchesType viewType;
  TeamColumn({this.viewType});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        viewType == MatchesType.NOT_PREDICTED || viewType == MatchesType.IN_PREDICTED ?  Text("+", style: TextStyles.StyleBold22(color: Colors.grey[800]),) : Text(""),
        Spacer(flex: 2),
        Image.asset(getImage("team2.png"), width: 50, height: 50,),
        Spacer(flex: 2),
        Text("Villarreal", style: TextStyles.StyleBold12(color: Colors.grey[900]),),
        Spacer(flex: 1),
        Text("Away", style: TextStyles.StyleBold8(color: Colors.grey[500]),),
        Spacer(flex: 2),
        viewType == MatchesType.NOT_PREDICTED || viewType == MatchesType.IN_PREDICTED ?  Text("–", style: TextStyles.StyleBold22(color: Colors.grey[800]),) : Text(""),
      ],);

  }
}

class ResultColumn extends StatelessWidget {
  final MatchesType viewType;
  ResultColumn({this.viewType});
  final List<Widget> pointWin = [
    Image.asset(getImage("favorite.png"), width: 14, height: 14,),
    SizedBox(width: 2,),
    Text("+3", style: TextStyles.StyleBold12(),)];

  Widget inPredictionMode(){
    return Column(
      children: <Widget>[
        Text("Allianz", style: TextStyles.StyleMeduim12(),),
        Spacer(flex: 2),
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Text("3", style: TextStyles.StyleBold28(),),
            SizedBox(width: 10,),
            Image.asset(getImage("favorite.png"), width: 14, height: 14,),
            SizedBox(width: 2,),
            Text("+3", style: TextStyles.StyleBold12(),),
            SizedBox(width: 10,),
            Text("1", style: TextStyles.StyleBold28(),),
          ],
        ),
        Spacer(flex: 1),
        Text("09 : 45 PM", style: TextStyles.StyleBold10(),),
        Spacer(flex: 1),
        Container(
            padding: EdgeInsets.symmetric(vertical: 4, horizontal: 20),
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(15)
            ),
            child: Text("Skip", style: TextStyles.StyleMeduim12(),))
      ],
    );
  }

  Widget inLiveMode(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text("Allianz", style: TextStyles.StyleMeduim12(),),
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Text("3", style: TextStyles.StyleBold28(),),
            SizedBox(width: 10,),
            Text(":", style: TextStyles.StyleBold28(color: Colors.grey[400]),),
            SizedBox(width: 10,),
            Text("1", style: TextStyles.StyleBold28(),),
          ],
        ),
        Container(
            padding: EdgeInsets.symmetric(vertical: 4, horizontal: 20),
            decoration: BoxDecoration(
                color: AppColors.darkRed,
                borderRadius: BorderRadius.circular(15)
            ),
            child: Text("• LIVE  55'", style: TextStyles.StyleMeduim12(color: Colors.white),))
      ],
    );
  }

  Widget inFinishedMode(){
    return Column(
      children: <Widget>[
        Text("Allianz", style: TextStyles.StyleMeduim12(),),
        Spacer(flex: 2),
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Text("3", style: TextStyles.StyleBold28(),),
            SizedBox(width: 10,),
            Image.asset(getImage("favorite.png"), width: 14, height: 14,),
            SizedBox(width: 2,),
            Text("+3", style: TextStyles.StyleBold12(),),
            SizedBox(width: 10,),
            Text("1", style: TextStyles.StyleBold28(),),
          ],
        ),

        Spacer(flex: 1),
        Text("Finished", style: TextStyles.StyleBold10(),),
        Spacer(flex: 1),
        Container(
            padding: EdgeInsets.symmetric(vertical: 4, horizontal: 20),
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(15)
            ),
            child: Text("3 - 2", style: TextStyles.StyleMeduim12(color: AppColors.darkRed),))
      ],
    );
  }

  Widget inWaitingPredictionMode(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text("Allianz", style: TextStyles.StyleMeduim12(),),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text("-", style: TextStyles.StyleBold28(),),
            SizedBox(width: 10,),
            Image.asset(getImage("favorite.png"), width: 14, height: 14,),
            SizedBox(width: 2,),
            Text("+3", style: TextStyles.StyleBold12(),),
            SizedBox(width: 10,),
            Text("-", style: TextStyles.StyleBold28(),),
          ],
        ),
        Text("09 : 45 PM", style: TextStyles.StyleBold10(),),
      ],
    );
  }


  @override
  Widget build(BuildContext context) {
    switch (viewType){
      case MatchesType.IN_PREDICTED:
        return inPredictionMode();
        break;
      case MatchesType.LIVE:
        return inLiveMode();
        break;
      case MatchesType.FINISHED:
        return inFinishedMode();
        break;

      case MatchesType.NOT_PREDICTED:
        return inWaitingPredictionMode();
        break;

      default:
        return null;
    }
  }
}
