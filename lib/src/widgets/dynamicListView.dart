import 'package:flutter/material.dart';

typedef ItemBuilder<T> = Widget Function(T item, bool isSelected);

class DynamicListView<T> extends StatefulWidget {
  final Axis direction;
  final List<T> data;
  final int defaultSelectedIndex;
  final double size;
  final Position position;
  final ItemBuilder<T> itemBuilder;
  final Function(int, T) onChange;

  const DynamicListView({
    @required this.data,
    this.direction = Axis.vertical,
    @required this.itemBuilder,
    this.defaultSelectedIndex = 0,
    this.position = Position.START,
    this.size,
    this.onChange
  })  : assert(data != null),
        assert(itemBuilder != null);

  @override
  _DynamicListViewState<T> createState() => _DynamicListViewState<T>();
}

class _DynamicListViewState<T> extends State<DynamicListView<T>> {
  int _selectedIndex;
  double _size;
  Position _position;
  ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
    this._selectedIndex = widget.defaultSelectedIndex;
    this._size = widget.size;
    this._position = widget.position;
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if(_size == null) return;
      scrollToSelectItem();
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: ScrollPhysics(),
      scrollDirection: widget.direction,
      itemCount: widget.data.length,
      controller: _controller,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () => setState(() {
            widget.onChange(index, widget.data[index]);
            return _selectedIndex = index;
          }),
          child: widget.itemBuilder(
            widget.data[index],
            index == _selectedIndex,
          ),
        );
      },
    );
  }

  void scrollToSelectItem(){
    double _itemRatio = 0;
    print(MediaQuery.of(context).size.width);
    print(_size);
    if(widget.direction == Axis.horizontal)
      _itemRatio = MediaQuery.of(context).size.width / _size;
    else
      _itemRatio = MediaQuery.of(context).size.height / _size;

    if(_selectedIndex > 0 && _size > 0){
      switch (_position) {
        case Position.START:
          _controller.jumpTo(_controller.offset + _size*(_selectedIndex));
          break;
        case Position.CENTER:
          _controller.jumpTo(_controller.offset + _size * (_selectedIndex - _itemRatio/2.25));
          break;
        case Position.END:
          _controller.jumpTo(_controller.offset + _size * (_selectedIndex + 1 - _itemRatio));
          break;
      }
    }
  }

}

enum Position{
  START, CENTER, END
}


//typedef void OnWidgetSizeChange(Size size);
//
//class MeasureSize extends StatefulWidget {
//  final Widget child;
//  final OnWidgetSizeChange onChange;
//
//  const MeasureSize({
//    Key key,
//    @required this.onChange,
//    @required this.child,
//  }) : super(key: key);
//
//  @override
//  _MeasureSizeState createState() => _MeasureSizeState();
//}
//
//class _MeasureSizeState extends State<MeasureSize> {
//  @override
//  Widget build(BuildContext context) {
//    SchedulerBinding.instance.addPostFrameCallback(postFrameCallback);
//
//    return Container(
//      key: widgetKey,
//      child: widget.child,
//    );
//
//  }
//
//  var widgetKey = GlobalKey();
//  var oldSize;
//
//  void postFrameCallback(_) {
//    var context = widgetKey.currentContext;
//    if (context == null) return;
//
//    var newSize = context.size;
//    if (oldSize == newSize) return;
//
//    oldSize = newSize;
//    widget.onChange(newSize);
//  }
//}
