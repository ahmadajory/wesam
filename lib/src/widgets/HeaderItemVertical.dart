import 'package:flutter/material.dart';
import 'package:app/util/TextStyles.dart';
class HeaderItemVertical extends StatelessWidget {
  final String name;
  final TextStyle style;
  HeaderItemVertical({@required this.name, this.style});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 12, top: 8, bottom: 8),
      child: Text(name, style: style == null ? TextStyles.StyleBold14(color: Colors.grey[700]) : style,),
    );
  }
}
