import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:app/util/Colors.dart';
import 'package:app/util/TextStyles.dart';
class DateItem extends StatelessWidget {
  final bool isActive;
  final DateTime dateTime;
  final formatter = DateFormat('E');

  DateItem({this.dateTime, this.isActive});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 40,
      height: 60,
      margin: EdgeInsets.only(right: 6),
      decoration: BoxDecoration(
        color: isActive ? AppColors.primary : Colors.white,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(formatter.format(dateTime), style: isActive ? TextStyles.StyleBold12() : TextStyles.Style12(color: Colors.grey[600]),),
          SizedBox(height: 5,),
          Text(dateTime.day.toString(), style: isActive ? TextStyles.StyleBold12() : TextStyles.Style12(color: Colors.grey[600]),)
        ],
      ),
    );
  }
}
