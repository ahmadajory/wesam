import 'package:flutter/material.dart';
import 'package:app/util/Colors.dart';
import 'package:app/util/TextStyles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HeaderItemHorizontal extends StatelessWidget {
  final String name;
  final bool isActive;
  HeaderItemHorizontal({this.name, this.isActive});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 12.w, right: 12.w),
      margin: EdgeInsets.only(right: 10.w),
      child: Center(child: Text(name, style: isActive ? TextStyles.StyleBold12() : TextStyles.Style12(color: Colors.grey[700]),)),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20.w),
        border: Border.all(width: 1, color: isActive ? AppColors.primary : Colors.grey[400]),
        color: isActive ? AppColors.primary : Colors.white,
      ),
    );
  }
}
