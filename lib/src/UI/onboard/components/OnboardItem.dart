import 'package:flutter/material.dart';
import 'package:app/util/TextStyles.dart';
import 'package:app/util/Assets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class OnBoardItem extends StatelessWidget {
  final OnBoard item;
  const OnBoardItem(this.item);
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Theme
          .of(context)
          .primaryColor,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Center(child: Image.asset(Assets.getImage(item.image), height: 270.h, fit: BoxFit.cover)),
            SizedBox(width: 10.w, height: 70.h,),
            Text(item.title, style: TextStyles.headStyleBold()),
            SizedBox(width: 10.w, height: 10.h,),
            Text(item.subTitle, style: TextStyles.subTitleStyle())
          ],
        ),
      ),
    );
  }
}


class OnBoard{
  String image, title, subTitle;
  OnBoard(this.image, this.title, this.subTitle);
}

List<OnBoard> OnBoradItems = [
  OnBoard("onboard_1.png", "Follow live soccer", "Stay up to date with live scores, match fixtures, results and league tables, and any other relevant information"),
  OnBoard("onboard_2.png", "Correct prediction is worth points", "Predict match results and earn points to compete with other football fans and unlock special badges"),
  OnBoard("onboard_3.png", "Find where to watch", "Find where your team’s matches are being screened nearby."),
  OnBoard("onboard_4.png", "Visits earn coins", "The more you visit the more you earn coins to unlock special vouchers"),
];
