import 'package:app/src/routes/Router.gr.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:app/util/Colors.dart';
import 'package:app/util/TextStyles.dart';
import 'package:app/util/AppButtons.dart';
import 'package:app/src/UI/onboard/components/OnboardItem.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class OnboardScreen extends StatefulWidget {
  static final String route = "/OnboardScreen";
  @override
  _OnboardScreenState createState() => _OnboardScreenState();
}

class _OnboardScreenState extends State<OnboardScreen> with SingleTickerProviderStateMixin{
  AnimationController _animationController;
  Animation<double> _nextPage;
  int _currentPage = 0;
  PageController _pageController = PageController(initialPage: 0);

  @override
  void initState() {
    super.initState();
    //Start at the controller and set the time to switch pages
    _animationController =
    new AnimationController(vsync: this, duration: Duration(seconds: 3));
    _nextPage = Tween(begin: 0.0, end: 1.0).animate(_animationController);

    //Add listener to AnimationController for know when end the count and change to the next page
    _animationController.addListener(() {
      if (_animationController.status == AnimationStatus.completed) {
        _animationController.reset(); //Reset the controller
        final int page = OnBoradItems.length; //Number of pages in your PageView
        if (_currentPage < page) {
          _currentPage++;
          _pageController.animateToPage(_currentPage,
              duration: Duration(milliseconds: 300), curve: Curves.easeInSine);
        } else {
          _currentPage = 0;
        }
      }
    });
  }

  @override
  void dispose() {
    _animationController.dispose();
    _pageController.dispose();
    print('dispose');
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    _animationController.forward();
    print(_nextPage.value);
    return Container(
      color: Theme.of(context).primaryColor,
      child: SafeArea(
        child: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Flexible(
                  flex: 1,
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Container(
                      width: 80.w,
                      height: 60.h,
                      child: FlatButton(
                        onPressed: navigateToLogin,
                        child: Text(
                          "Skip",
                          style: TextStyles.buttonStyle(),
                        ),
                      ),
                    ),
                  ),
                ),
                Flexible(
                  flex: 8,
                  child: Container(
                    child: PageView.builder(
                        itemCount: 4,
                        scrollDirection: Axis.horizontal,
                        controller: _pageController,
                        onPageChanged: (value) {
                          //When page change, start the controller
                          _animationController.forward();
                        },
                        itemBuilder: (BuildContext context, int index) {

                          return OnBoardItem(OnBoradItems[index]);
                        }),


                  ),
                ),
                Flexible(
                    flex: 1,
                    child: Container(
                      child: Align(
                        alignment: Alignment(0.8.w, -1.h),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            SmoothPageIndicator(
                                controller: _pageController,
                                count: OnBoradItems.length,
                                effect: ExpandingDotsEffect(
                                    expansionFactor: 3,
                                    dotWidth: 10.w,
                                    dotHeight: 10.w,
                                    activeDotColor: AppColors.black,
                                    dotColor:
                                        AppColors.black.withOpacity(0.5))),
                            SizedBox(
                              height: 10.h,
                            ),
                            NormalButton(
                              horizontalPadding: 30,
                              verticalPadding: 10,
                              text: "Join Now",
                              onPressed: navigateToLogin,
                              fillColor: Colors.white,
                              splashColor: Colors.grey[100],
                            ),
                          ],
                        ),
                      ),
                    )),
                SizedBox(
                  height: 30.h,
                )
              ]),
        ),
      ),
    );
  }

  void navigateToLogin() {
    ExtendedNavigator.root.replace(Routes.loginScreen);
  }
}
