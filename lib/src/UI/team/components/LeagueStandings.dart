import 'package:app/src/widgets/dynamicListView.dart';
import 'package:app/util/Assets.dart';
import 'package:app/util/TextStyles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'HeaderStandings.dart';

class LeagueStandings extends StatelessWidget {
  final List<int> standingsData;
  LeagueStandings(this.standingsData);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        HeaderStandings(),
        SizedBox(height: 5.h,),
        Expanded(
          child: DynamicListView(data: standingsData,
            itemBuilder: (int item, bool isSelected) => standingsItem(item),),
        )
      ],
    );
  }

  Widget standingsItem(int index){
    return Container(height: 35.h,
      padding: EdgeInsets.only(bottom: 15.h),
      child: Row(crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(flex: 4 ,child: Wrap(
            crossAxisAlignment: WrapCrossAlignment.center,
            children: <Widget>[
              Text((index).toString(),style: TextStyles.StyleMeduim12(),),
              SizedBox(width: 8.w,),
              Image.asset(getImage("realmadrid.png"), width: 18.w,height: 18.w,),
              SizedBox(width: 8.w,),
              Text("Real Madrid",style: TextStyles.StyleMeduim12(),),
            ],
          )),
          Expanded(flex: 1 ,child: Text("6",style: TextStyles.StyleMeduim12(),)),
          Expanded(flex: 1 ,child: Text("6",style: TextStyles.StyleMeduim12(),)),
          Expanded(flex: 1 ,child: Text("5",style: TextStyles.StyleMeduim12(),)),
          Expanded(flex: 1 ,child: Text("12",style: TextStyles.StyleMeduim12(),)),
          Expanded(flex: 1 ,child: Text("20",style: TextStyles.StyleMeduim12(),)),
          Expanded(flex: 1 ,child: Text("12",style: TextStyles.StyleMeduim12(),)),
        ],),);
  }
}
