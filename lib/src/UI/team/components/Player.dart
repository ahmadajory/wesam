import 'package:app/util/Assets.dart';
import 'package:app/util/TextStyles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PlayerWidget extends StatefulWidget {
  @override
  _PlayerWidgetState createState() => _PlayerWidgetState();
}

class _PlayerWidgetState extends State<PlayerWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(height: 62.h, margin: EdgeInsets.only(bottom: 10.h),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 60.w,
            height: 60.w,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.r),
                image: DecorationImage(
                    image: ExactAssetImage(getImage("messy.jpeg")),
                    fit: BoxFit.contain
                )
            ),
          ),
          SizedBox(width: 10.w,),
          Wrap(
            direction: Axis.vertical ,
            children: <Widget>[
              Text("Lionel Messi", style: TextStyles.StyleMedium14(color: Colors.grey[900]),),
              SizedBox(height: 6.h,),
              Text("Forward", style: TextStyles.StyleMeduim12(color: Colors.grey[400]),),
            ],),
          Spacer(flex: 1,),
          Image.asset(getImage("ball.png"), width: 20.w, height: 20.w,),
          SizedBox(width: 8.w,),
          Text("19", style: TextStyles.StyleMedium14(color: Colors.grey[1000]),)
        ],),);
  }
}
