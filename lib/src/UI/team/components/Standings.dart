import 'package:app/src/UI/team/GroupsStandings.dart';
import 'package:app/src/widgets/HeaderItemHorizontal.dart';
import 'package:app/src/widgets/dynamicListView.dart';
import 'package:flutter/material.dart';
import 'package:app/src/models/Group.dart';
import 'LeagueStandings.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class StandingsScreen extends StatefulWidget {
  @override
  _StandingsScreenState createState() => _StandingsScreenState();
}

class _StandingsScreenState extends State<StandingsScreen> {
  final List<String> leagues = ["Champions league", "La Liga"];
  bool isGroup = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10.w, top: 15.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            height: 30.h,
            child: DynamicListView<String>(data: leagues,
              itemBuilder: (name, isActive) => HeaderItemHorizontal(name: name, isActive: isActive,),
              defaultSelectedIndex: 0, direction: Axis.horizontal,
              onChange: (index, item) => onChange(index, item),
            ),

              ),
          SizedBox(height: 20.h),
          !isGroup ? Expanded(child: LeagueStandings(List.generate(30, (index) => index + 1))) :
                    Expanded(child: GroupsStandings(data: groupsStandingsData,)),
        ]) ,

    );
  }

  void onChange(int index, String item){
    setState(() {
      index == 0 ? isGroup = true : isGroup = false;
    });
  }
}


