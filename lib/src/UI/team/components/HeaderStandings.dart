import 'package:app/util/TextStyles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HeaderStandings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Wrap(
      children: <Widget>[

        Container(height: 20.h,
          child: Row(crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(flex: 4 ,child: Text("Club",style: TextStyles.StyleMeduim12(),)),
              Expanded(flex: 1 ,child: Text("MP",style: TextStyles.StyleMeduim12(),)),
              Expanded(flex: 1 ,child: Text("W",style: TextStyles.StyleMeduim12(),)),
              Expanded(flex: 1 ,child: Text("D",style: TextStyles.StyleMeduim12(),)),
              Expanded(flex: 1 ,child: Text("L",style: TextStyles.StyleMeduim12(),)),
              Expanded(flex: 1 ,child: Text("G",style: TextStyles.StyleMeduim12(),)),
              Expanded(flex: 1 ,child: Text("PT",style: TextStyles.StyleMeduim12(),)),
            ],),),
        Divider(height: 5.h,),
      ],
    );
  }
}
