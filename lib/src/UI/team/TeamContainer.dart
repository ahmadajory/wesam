import 'package:app/src/models/Match.dart';
import 'package:app/src/UI/team/Players.dart';
import 'package:flutter/material.dart';
import 'package:app/util/TextStyles.dart';
import 'package:app/util/Colors.dart';
import 'package:app/src/UI/home/Matches.dart';
import 'package:app/util/Assets.dart';
import 'package:app/src/UI/team/components/Standings.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TeamContainerScreen extends StatefulWidget {
  @override
  _TeamContainerScreenState createState() => _TeamContainerScreenState();
}

class _TeamContainerScreenState extends State<TeamContainerScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Team Details"),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: Colors.white,
      body: Container(
        child: Column(
          children: <Widget>[
            Container(height: 100.h,
            padding: EdgeInsets.only(left: 25.w),
            color: Colors.white,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(width: 102.h,height: 102.h,
                    decoration: BoxDecoration(
                        color: Colors.green[700].withOpacity(0.2),
                        borderRadius: BorderRadius.circular(51.h)
                    ),
                    child: Center(child: Image.asset(getImage("realmadrid.png"), width: 60.h, height: 60.h,)),),
                    Positioned(
                      right: 0, top: 0,
                        child: Container(width: 28.h,height: 28.h,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(14.h),
                          color: AppColors.primary,
                          border: Border.all(color: Colors.white, width: 2)
                        ),
                        child: Center(child: Icon(Icons.star_border, size: 16.h,)),
                        ))
                  ],
                ),
                SizedBox(width: 20.w,),
                Wrap(
                  direction: Axis.vertical,
                  children: <Widget>[
                    Text("F.C Barcaluna", style: TextStyles.StyleMedium22(),),
                    Text("Camp Nou", style: TextStyles.Style12(color: Colors.grey[400]),),
                  ],
                )
              ],
            ),),
            SizedBox(height: 20.h,),
            Expanded(
              child: DefaultTabController(
                length: 3,
                child: Scaffold(
                  backgroundColor: Colors.white,
                  appBar: TabBar(
                    unselectedLabelColor: Colors.black,
                    isScrollable: true,
                    labelPadding: EdgeInsets.only(left: 25.w, right: 10.w, bottom: 6.h),
                    labelStyle: TextStyles.StyleBold14(),
                    indicatorColor: AppColors.primary,
                    indicatorWeight: 3,
                    indicatorSize: TabBarIndicatorSize.tab,
                    indicatorPadding: EdgeInsets.only(left: 20.w),
                    tabs: <Widget>[
                      Text("MATCHES",style: TextStyles.StyleNormal16()),
                      Text("STANDINGS",style: TextStyles.StyleNormal16()),
                      Text("PLAYERS",style: TextStyles.StyleNormal16()),
                    ],
                  ),
                  body: TabBarView(
                    children: [
                      MatchesScreen(matches: allMatchesForTeam, color: Colors.white, headerStyle: TextStyles.StyleMedium20(),),
                      StandingsScreen(),
                      PlayersScreen(),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

//import 'package:app/src/models/Match.dart';
//import 'package:app/src/UI/team/Players.dart';
//import 'package:flutter/material.dart';
//import 'package:app/util/TextStyles.dart';
//import 'package:app/util/Colors.dart';
//import 'package:app/src/UI/home/Matches.dart';
//import 'package:app/util/Assets.dart';
//import 'package:app/src/UI/team/Standings.dart';
//class TeamContainerScreen extends StatefulWidget {
//  @override
//  _TeamContainerScreenState createState() => _TeamContainerScreenState();
//}
//
//class _TeamContainerScreenState extends State<TeamContainerScreen> {
//  @override
//  Widget build(BuildContext context) {
//    return DefaultTabController(
//      length: 3,
//      child: Scaffold(
//          backgroundColor: Colors.white,
//          appBar: AppBar(
//              elevation: 0,
//              backgroundColor: Colors.white,
//              flexibleSpace: Container(height: 200,
//                padding: EdgeInsets.only(top: 20, left: 25),
//                color: Colors.white,
//                child: Row(
//                  crossAxisAlignment: CrossAxisAlignment.center,
//                  children: <Widget>[
//                    Stack(
//                      children: <Widget>[
//                        Container(width: 102, height: 102,
//                          decoration: BoxDecoration(
//                              color: Colors.green[700].withOpacity(0.2),
//                              borderRadius: BorderRadius.circular(51)
//                          ),
//                          child: Center(child: Image.asset(
//                            getImage("realmadrid.png"), width: 60,
//                            height: 60,)),),
//                        Positioned(
//                            right: 0, top: 0,
//                            child: Container(width: 28, height: 28,
//                              decoration: BoxDecoration(
//                                  borderRadius: BorderRadius.circular(14),
//                                  color: AppColors.primary,
//                                  border: Border.all(color: Colors.white,
//                                      width: 2)
//                              ),
//                              child: Center(child: Icon(Icons.star_border,
//                                size: 16,)),
//                            ))
//                      ],
//                    ),
//                    SizedBox(width: 20,),
//                    Wrap(
//                      direction: Axis.vertical,
//                      children: <Widget>[
//                        Text(
//                          "F.C Barcaluna", style: TextStyles.StyleMedium22(),),
//                        Text("Camp Nou",
//                          style: TextStyles.Style12(color: Colors.grey[400]),),
//                      ],
//                    )
//                  ],
//                ),),
//              bottom: TabBar(
//                unselectedLabelColor: Colors.black,
//                isScrollable: true,
//                labelPadding: EdgeInsets.only(left: 25, right: 10, bottom: 6),
//                labelStyle: TextStyles.StyleBold14(),
//                indicatorColor: AppColors.primary,
//                indicatorWeight: 3,
//                indicatorSize: TabBarIndicatorSize.tab,
//                indicatorPadding: EdgeInsets.only(left: 20),
//                tabs: <Widget>[
//                  Text("MATCHES"),
//                  Text("STANDINGS"),
//                  Text("PLAYERS"),
//                ],
//
//              )),
//          body: TabBarView(
//            children: [
//              MatchesScreen(matches: allMatchesForTeam,
//                color: Colors.white,
//                headerStyle: TextStyles.StyleMedium20(),),
//              StandingsScreen(),
//              PlayersScreen(),
//            ],
//          )
//
//      ),
//    );
//  }
//}
