import 'package:app/src/widgets/dynamicListView.dart';
import 'package:app/util/Assets.dart';
import 'package:app/util/TextStyles.dart';
import 'package:flutter/material.dart';
import 'package:app/src/UI/team/components/Player.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PlayersScreen extends StatefulWidget {
  @override
  _PlayersScreenState createState() => _PlayersScreenState();
}

class _PlayersScreenState extends State<PlayersScreen> {
  bool isGroup = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 25.w, right: 25.w, top: 15.h),
      child: DynamicListView<int>(data: List.generate(20, (index) => index),
        itemBuilder: (name, isActive) => PlayerWidget(),
        defaultSelectedIndex: 0, direction: Axis.vertical,
      ) ,

    );
  }

}




