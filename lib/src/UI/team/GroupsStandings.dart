import 'package:app/src/models/Group.dart';
import 'package:app/util/Assets.dart';
import 'package:app/util/TextStyles.dart';
import 'package:flutter/material.dart';
import 'package:app/src/widgets/dynamicListView.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'components/HeaderStandings.dart';

class GroupsStandings extends StatelessWidget {
  final List<Group> data;
  GroupsStandings({this.data});
  @override
  Widget build(BuildContext context) {
    return DynamicListView(data: data, itemBuilder: (item, isActive) => GroupStandings(group: item),);
  }
}

class GroupStandings extends StatefulWidget {
  final Group group;
  GroupStandings({this.group});
  @override
  _GroupStandingsState createState() => _GroupStandingsState();
}

class _GroupStandingsState extends State<GroupStandings> {

  List<Widget> _standingsWidgets = [];

  @override
  void initState() {
    super.initState();
    widget.group.standingsData.forEach((element) {
      _standingsWidgets.add(standingsItem(element));
    });
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(widget.group.name, style: TextStyles.StyleMedium20(color: Colors.grey[700]),),
        SizedBox(height: 15.h,),
        HeaderStandings(),
        SizedBox(height: 10.h,),
        Column(children: _standingsWidgets,),
        SizedBox(height: 20.h,)
      ],
    );
  }

  Widget standingsItem(int index){
    return Container(height: 28.h,
      padding: EdgeInsets.only(bottom: 8.h),
      child: Row(crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(flex: 4 ,child: Wrap(
            crossAxisAlignment: WrapCrossAlignment.center,
            children: <Widget>[
              Text((index).toString(),style: TextStyles.StyleMeduim12(),),
              SizedBox(width: 8.w,),
              Image.asset(getImage("realmadrid.png"), width: 1.w,height: 18.w,),
              SizedBox(width: 8.w,),
              Text("Real Madrid",style: TextStyles.StyleMeduim12(),),
            ],
          )),
          Expanded(flex: 1 ,child: Text("6",style: TextStyles.StyleMeduim12(),)),
          Expanded(flex: 1 ,child: Text("6",style: TextStyles.StyleMeduim12(),)),
          Expanded(flex: 1 ,child: Text("5",style: TextStyles.StyleMeduim12(),)),
          Expanded(flex: 1 ,child: Text("12",style: TextStyles.StyleMeduim12(),)),
          Expanded(flex: 1 ,child: Text("20",style: TextStyles.StyleMeduim12(),)),
          Expanded(flex: 1 ,child: Text("12",style: TextStyles.StyleMeduim12(),)),
        ],),);
  }

}
