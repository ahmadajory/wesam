import 'package:app/src/routes/Router.gr.dart';
import 'package:app/util/Colors.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:app/util/TextStyles.dart';
import 'package:app/util/Assets.dart';
import 'package:app/util/BottomNavBar.dart';
import 'package:app/src/UI/home/HomeContainer.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MainContainerScreen extends StatefulWidget {
  @override
  _MainContainerScreenState createState() => _MainContainerScreenState();
}

class _MainContainerScreenState extends State<MainContainerScreen> {
  var selectedTab = 0;
  final List<Widget> icons = [
    ImageIcon(AssetImage(getImage("ball.png")),size: 17.h,),
    Icon(Icons.location_on,),
    Icon(Icons.person),
  ];

  final texts = [
    "Matches",
    "Explore",
    "Profile",
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Material(
        elevation: 10,
        color: Colors.white,
        child: Padding(
            padding: EdgeInsets.only(bottom: NarBarConfigs.BOTTOM_PADDING,left: NarBarConfigs.WIDTH_PADDING, right: NarBarConfigs.WIDTH_PADDING),
            child: CustomBottomNavigatorBar(icons: icons, texts: texts,
              onChange: (val){
                setState(() {
                  selectedTab = val;
                });
              },)
        ),
      ),

      appBar: AppBar(
          titleSpacing: 0,
          automaticallyImplyLeading: false,
          elevation: 0,
          backgroundColor: Colors.white,
          title: Container(
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding:  EdgeInsets.only(right: 20.w, left: 20.w),
              child: Row(
                children: <Widget>[
                  IconButton(
                    icon: Image.asset(getImage("notification.png"), width: 26.w,),
                    onPressed: (){
                      print("notifications");
                    },),
                  Spacer(flex: 1,),
                  GestureDetector(
                    onTap: (){
                      print("Shield");
                    },
                    child: Row(children: <Widget>[
                      Image.asset(getImage("shield.png"), height: 24.h,),
                      SizedBox(width: 8.w,),
                      Text("2", style: TextStyles.StyleBold16(),)
                    ],),
                  ),

                  SizedBox(width: 20.w,),

                  GestureDetector(
                    onTap: (){
                      print("Favorite");
                    },
                    child: Row(children: <Widget>[
                      Image.asset(getImage("favorite.png"), height: 20.h,),
                      SizedBox(width: 8.w,),
                      Text("2", style: TextStyles.StyleBold16(),)
                    ],),
                  ),

                ],
              ),
            ),
          ),),

      body: Container(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 20.w, right: 20.w, top: 20.h),
              child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("Today`s\nmatches for you", style: TextStyles.StyleBold22(),),
                    Spacer(flex: 1,),
                    GestureDetector(
                      onTap: navigateToTeam,
                        child: Text("View All", style: TextStyles.StyleBold14(color: Colors.grey[500]),))
                  ],
              ),
            ),
            SizedBox(height: 10.h,),
            Flexible(flex: 1,
            child:
            selectedTab == 0 ?
                HomeContainerScreen() :
            selectedTab == 1 ?
                  Container(
                    color: Colors.blue,
                  ) :
                  Container(
                    color: Colors.red,
                  ) ,)
          ],
        ),
      ),

    );
  }

  void navigateToTeam(){
    ExtendedNavigator.root.push(Routes.teamContainerScreen);
  }
  // void navigateToAllMatches(){
  //   ExtendedNavigator.root.push(Routes);
  // }
}


