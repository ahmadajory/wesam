import 'package:app/src/routes/Router.gr.dart';
import 'package:app/util/Colors.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:app/util/TextStyles.dart';
import 'package:app/util/AppButtons.dart';
import 'package:app/util/Assets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.white,
//      resizeToAvoidBottomInset: false,
//      resizeToAvoidBottomPadding: false,
        child: SafeArea(
          child: Padding(
              padding: EdgeInsets.only(left: 20.w, right: 20.w),
              child: Column(
//          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Align(
                      alignment: Alignment.centerRight,
                      child: Container(
                          width: 80.w,
                          height: 50.h,
                          child: FlatButton(
                              onPressed: navigateToInBoard,
                              child: Text(
                                "Skip",
                                style: TextStyles.buttonStyle(color:  AppColors.LightBlack),
                              )))),
                  Row(
                    children: <Widget>[
                      Container(
                          width: 104.h,
                          height: 104.h,
                          padding:  EdgeInsets.all(4.0.h),
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: AppColors.LightBlack, width: 3.h),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(52.h))),
                          child: CircleAvatar(
                              backgroundColor: AppColors.primary,
                              child: Image.asset(
                                getImage("photo-camera.png"),
                                width: 31.w,
                                height: 26.h,
                              ))),
                      SizedBox(
                        width: 12.w,
                      ),
                      Flexible(
                        flex: 1,
                        child: Column(
//                  direction: Axis.vertical,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Smile, add a photo!",
                              style: TextStyles.subTitleStyle(color:  AppColors.LightBlack),
                            ),
                            SizedBox(
                              height: 12.h,
                            ),
                            Text(
                              "Your photo will be attached on the ranking leaderboard!",
                              style:
                                  TextStyles.smallText(color: Colors.grey[500]),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 25.h,
                  ),
                  Padding(
                    padding:  EdgeInsets.only(left: 10.w, right: 10.w),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              "Full Name",
                              style:
                                  TextStyles.smallText(color: Colors.grey[500]),
                            ),
                            Container(
                              height: 48.h,
                              child: TextFormField(
                                  decoration: InputDecoration(
                                      hintText: "Please enter your full name",
                                      hintStyle: TextStyles.smallText(
                                          color: Colors.grey[400]))),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20.h,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Your Birthday",
                              style:
                                  TextStyles.smallText(color: Colors.grey[500]),
                            ),
                            Container(
                              height: 48.h,
                              child: TextFormField(

                                  decoration: InputDecoration(
                                      hintText:
                                          "We`ll give you a sweet birthday treat.",
                                      hintStyle: TextStyles.smallText(
                                          color: Colors.grey[400]))),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20.h,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          // mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              "Gender",
                              style:
                                  TextStyles.smallText(color: Colors.grey[500]),
                            ),
                            Container(
                              height: 48.h,
                              child: TextFormField(
                                  decoration: InputDecoration(
                                      hintText:
                                          "We`ll give you better recommendations.",
                                      hintStyle: TextStyles.smallText(
                                          color: Colors.grey[400]))),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 30.h,
                        ),
                        Center(
                          child: Wrap(
                            children: <Widget>[
                              Text(
                                "GET THE ",
                                style: TextStyles.StyleLight18(color:  AppColors.LightBlack),
                              ),
                              Text("BEST EXPERIENCE",
                                  style: TextStyles.StyleBold18(color:  AppColors.LightBlack))
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 12.h,
                        ),
                        Flexible(
                          fit: FlexFit.loose,
                          child: Text(
                            "You’ll get to compete your friends, checkout where they watch the match. We’ll never post anything on your behalf",
                            style: TextStyles.StyleNormal12(
                                color: AppColors.LightBlack),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        SizedBox(height: 20.h),
                        Padding(
                          padding:  EdgeInsets.only(left: 40.w, right: 40.w),
                          child: NormalButtonWithIcon(
                            text: "Connect your facebook",
                            textStyle: TextStyles.buttonStyle(color: AppColors.white),
                            fillColor: AppColors.facebookButtonColor,
                            image: Image.asset(
                              getImage("facebook.png"),
                              width: 20.h,
                              height: 20.h,
                              color: Colors.white,
                            ),
                            onPressed: () {},
                          ),
                        ),
                        SizedBox(
                          height: 109.h,
                        ),
                        NormalButton(
                          horizontalPadding: 30,
                          verticalPadding: 10,
                          text: "Next",
                          onPressed: navigateToFavorite,
                          splashColor: Colors.grey[100],
                        ),
                        SizedBox(
                          height: 20.h,
                        ),
                      ],
                    ),
                  )
                ],
              )),
        ));
  }

  void navigateToInBoard() {
    ExtendedNavigator.root.replace(Routes.onboardScreen);
  }

  void navigateToFavorite() {
    ExtendedNavigator.root.replace(Routes.favoriteScreen);
  }
}
