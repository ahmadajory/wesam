import 'package:app/src/routes/Router.gr.dart';
import 'package:app/util/Colors.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:app/util/TextStyles.dart';
import 'package:app/util/AppButtons.dart';
import 'package:app/util/Assets.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class VerifyScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Theme.of(context).primaryColor,
//      resizeToAvoidBottomInset: false,
//      resizeToAvoidBottomPadding: false,
      child: SafeArea(
        bottom: false,
        child: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Flexible(
                    flex: 1,
                    child: Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                            width: 90.w,
                            height: 60.h,
                            child: FlatButton(
                                onPressed: navigateToInBoard,
                                child: Text(
                                  "Skip",
                                  style: TextStyles.buttonStyle(),
                                ))))),
                Flexible(
                  flex: 4,
                  child: Image.asset(
                    getImage("verify_bg.png"),
                    fit: BoxFit.cover,
                  ),
                ),
                Spacer(
                  flex: 1,
                ),
                Flexible(
                    flex: 2,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 0, horizontal: 20.w),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                "Verification",
                                style: TextStyles.headStyleBold(),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20.h,
                          ),
                          Row(
                            children: <Widget>[
                              Text(
                                "Needed to activate your account and ",
                                style: TextStyles.smallText(),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 8.h,
                          ),
                          Text(
                            "keep it secured,",
                            style: TextStyles.smallText(),
                          ),
                        ],
                      ),
                    )),
                Flexible(
                    flex: 5,
                    child: Container(
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.25),
                              spreadRadius: 4,
                              blurRadius: 20,
                              offset: Offset(0.w, 3.h),
                            )
                          ],
                          borderRadius: BorderRadius.only(
                            topLeft:  Radius.circular(40.r),
                            topRight:  Radius.circular(40.r),
                          ),
                        ),
                        child: SafeArea(
                          top: false,
                          left: false,
                          right: false,
                          child: Padding(
                            padding:  EdgeInsets.fromLTRB(20.w, 40.h, 20.w, 0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                Text(
                                  'Enter A 4 Digit Number That',
                                  style: TextStyles.smallText(),
                                ),
                                SizedBox(
                                  height: 8.h,
                                ),
                                Wrap(
                                  children: <Widget>[
                                    Text(
                                      'Was Sent To',
                                      style: TextStyles.smallText(),
                                    ),
                                    SizedBox(
                                      width: 8.w,
                                    ),
                                    Text(
                                      '+ (966) 59 88 72 444',
                                      style: TextStyles.smallTextBold(),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 20.h,
                                ),
                                Padding(
                                  padding:  EdgeInsets.symmetric(
                                      vertical: 0, horizontal: 16.w),
                                  child: PinCodeTextField(
                                    length: 4,
                                    obsecureText: false,
                                    animationType: AnimationType.fade,

                                    pinTheme: PinTheme(
                                      shape: PinCodeFieldShape.underline,
                                      selectedColor: Colors.grey[600],
                                      selectedFillColor: Colors.white,
                                      activeColor: Colors.grey[400],
                                      activeFillColor: Colors.white,
                                      inactiveColor: Colors.grey[400],
                                      inactiveFillColor: Colors.white,


//                                borderRadius: BorderRadius.circular(5),
//                                fieldHeight: 50,
                                      fieldWidth: 50.w,
//                                activeFillColor: Colors.white,
                                    ),
                                    animationDuration:
                                        Duration(milliseconds: 300),
//                              backgroundColor: Colors.blue.shade50,
                                    enableActiveFill: true,
                                    onCompleted: (v) {
                                      print("Completed");
                                    },
                                    onChanged: (value) {
                                      print(value);
                                    },
                                    beforeTextPaste: (text) {
                                      print("Allowing to paste $text");
                                      //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                                      //but you can show anything you want here, like your pop up saying wrong paste format or etc
                                      return true;
                                    },
                                  ),
                                ),
                                SizedBox(
                                  height: 20.h,
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 0, horizontal: 16.w),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        're-send code in 0:59',
                                        style: TextStyles.smallTextMedium(),
                                      ),
                                      NormalButton(
                                        horizontalPadding: 30,
                                        verticalPadding: 10,
                                        text: "Verify",
                                        onPressed: navigateToProfile,
                                        splashColor: Colors.grey[100],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )),
              ]),
        ),
      ),
    );
  }

  void navigateToInBoard() {
    ExtendedNavigator.root.replace(Routes.onboardScreen);
  }

  void navigateToProfile() {
    ExtendedNavigator.root.replace(Routes.profileScreen);
  }
}
