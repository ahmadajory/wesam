import 'package:app/src/routes/Router.gr.dart';
import 'package:app/util/Colors.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:app/util/TextStyles.dart';
import 'package:app/util/AppButtons.dart';
import 'package:app/util/Assets.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FavoriteScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Material(
          color: Colors.grey[100],
//      resizeToAvoidBottomInset: false,
//      resizeToAvoidBottomPadding: false,
          child: SafeArea(
            child: Column(
//          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Padding(
                  padding:  EdgeInsets.only(left: 20.w, right: 20.w, top: 25.h),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Text("Choose your\nTeams & Leagues", style: TextStyles.StyleBold22(),),
                          Container(
                            width: 90.w,
                            height: 35.h,
                            child: FlatButton(materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                onPressed: () {}, child: Text("Skip", style: TextStyles.buttonStyle(),)),
                          ),
                        ],
                      ),
                      SizedBox(height: 20.h,),
                      Padding(
                        padding:  EdgeInsets.only(left: 8.w, right: 18.w),
                        child: Material(
                          shadowColor: Colors.white,
                          shape: StadiumBorder(),
                          elevation: 5.0,
                          child: TextFormField(
                            autofocus: false,
                            decoration: InputDecoration(
                              hintStyle: TextStyles.StyleNormal12(color: Colors.grey[500]),
                              hintText: "Search your league or team...",
                              prefixIcon: Icon(Icons.search),
                              filled: true,
                              fillColor: Colors.white,
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                  borderRadius: BorderRadius.circular(25.7.r),
                                ),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(25.7.r),
                              ),

                            ),),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 20.h,),
                Container(
                  height: 160.h,
                  child: ListView(
                    // This next line does the trick.
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      LeagueItem(),
                      LeagueItem(),
                      LeagueItem(),
                      LeagueItem(),
                      LeagueItem(),
                      LeagueItem(),
                      LeagueItem(),
                      LeagueItem(),
                    ],
                  ),
                ),
                SizedBox(height: 20.h,),
                Flexible(
                  flex: 1,
                  child: Padding(
                    padding:  EdgeInsets.only(left: 20.w, right: 20.w),
                    child: ListView(
                      scrollDirection: Axis.vertical,
                      children: <Widget>[
                        TeamItem(),
                        TeamItem(),
                        TeamItem(),
                        TeamItem(),
                        TeamItem(),
                        TeamItem(),
                        TeamItem(),
                      ],
                    ),
                  ),
                )


              ],
            ),
          )),
      // floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
      floatingActionButton: Align(
        alignment: Alignment(.15 ,1),
          child: NormalButton(text: "Get Started", horizontalPadding: 30, verticalPadding: 10, onPressed: navigateToHome,),
    ));
  }

  void navigateToHome(){
    ExtendedNavigator.root.replace  (Routes.mainContainerScreen);
  }
}

class LeagueItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container( width: 160.w,
            decoration: BoxDecoration(
              color: Colors.white,
                boxShadow: [
                  new BoxShadow(
                      color: Colors.grey.withOpacity(0.1),
                      blurRadius: 1.0,
                      spreadRadius: 1
                  ),]
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Flexible(
                  flex: 1,
                  child: Align(alignment: Alignment(0.9,0.3),child:
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Icon(Icons.star_border, size: 15.h,),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black, width: 1.w),
                          borderRadius: BorderRadius.circular(20.r),
                        )
                    ),
              ),
                ),
              Flexible( flex: 3,child: Center(child: Image.asset(getImage("LaLiga.png"),fit: BoxFit.fitWidth,))),
              SizedBox(height: 5.h,),
              Flexible(flex: 1,child: Text("Spanish 1st League", style: TextStyles.smallText(color: Colors.grey[900]),),)

        ],),),
        SizedBox(width: 20.w,)
      ],
    );
  }
}

class TeamItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(height: 70.h,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(7.r),
              boxShadow: [
                new BoxShadow(
                  color: Colors.grey.withOpacity(0.2),
                  blurRadius: 2.0,
                  spreadRadius: 1
                ),]),
          child: Padding(
            padding:  EdgeInsets.only(left: 20.w, right: 15.w),
            child: Row(
              children: <Widget>[
                Padding(padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 1.w), child: Image.asset(getImage("realmadrid.png"))),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                  Text("Real Madrid", style: TextStyles.StyleBold14(color: Colors.grey[900]),),
                  SizedBox(height: 5.h,),
                  Text("Santiago Bernabeu", style: TextStyles.StyleNormal10(color: Colors.grey[500]),),
                ],),
                Spacer(flex: 1,),
                Container(
                    padding: EdgeInsets.all(5),
                    child: Icon(Icons.star_border, size: 15.h,),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black, width: 1.h),
                      borderRadius: BorderRadius.circular(20.r),
                    )
                ),
              ],
            ),
          ),
        )
        ,SizedBox(height: 15.h,)],);

  }
}

