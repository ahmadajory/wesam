import 'package:app/src/routes/Router.gr.dart';
import 'package:app/util/Colors.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:app/util/TextStyles.dart';
import 'package:app/util/AppButtons.dart';
import 'package:app/util/Assets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Theme.of(context).primaryColor,
//      resizeToAvoidBottomInset: false,
//      resizeToAvoidBottomPadding: false,
      child: SafeArea(
        bottom: false,
        child: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Flexible(
                    flex: 1,
                    child: Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                            width: 80.w,
                            height: 60.h,
                            child: FlatButton(
                                onPressed: navigateToInBoard,
                                child: Text(
                                  "Skip",
                                  style: TextStyles.buttonStyle(),
                                ))))),
                Flexible(
                  flex: 4,
                  child: Image.asset(
                    getImage("login_bg.png"),
                    fit: BoxFit.cover,
                  ),
                ),
                Spacer(
                  flex: 1,
                ),
                Flexible(
                    flex: 2,
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 0, horizontal: 20.w),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                "Hello",
                                style: TextStyles.headStyleLight(),
                              ),
                              Text(
                                " Champion,",
                                style: TextStyles.headStyleBold(),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20.h,
                          ),
                          Row(
                            children: <Widget>[
                              Text(
                                "Think you know football ? ",
                                style: TextStyles.smallText(),
                              ),
                              Text(
                                "Prove it!",
                                style: TextStyles.smallTextBold(),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 8.h,
                          ),
                          Text(
                            "Enter your mobile number and join now,",
                            style: TextStyles.smallText(),
                          ),
                        ],
                      ),
                    )),
                Flexible(
                    flex: 5,
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.25),
                            spreadRadius: 4,
                            blurRadius: 20,
                            offset: Offset(0.w, 3.h),
                          )
                        ],
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40.r),
                          topRight: Radius.circular(40.r),
                        ),
                      ),
                      child: SafeArea(
                        top: false,
                        left: false,
                        right: false,
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(20.w, 40.h, 20.w, 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Wrap(
                                children: <Widget>[
                                  Text(
                                    'We will send you ',
                                    style: TextStyles.smallText(),
                                  ),
                                  Text(
                                    'One Time Password',
                                    style: TextStyles.smallTextBold(),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 8.h,
                              ),
                              Text(
                                'on this mobile number',
                                style: TextStyles.smallText(),
                              ),
                              SizedBox(
                                height: 20.h,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Enter Mobile Number',
                                    style: TextStyles.smallText(
                                        color: Colors.grey[400]),
                                  ),
                                  Column(
                                    children: <Widget>[
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            child: Padding(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 8.h,
                                                  horizontal: 16.w),
                                              child: Text('SA +966',
                                                  style:
                                                      TextStyles.smallText()),
                                            ),
                                            height: 30.h,
                                            decoration: BoxDecoration(
                                                color: AppColors.primary,
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        30.r)),
                                          ),
                                          SizedBox(
                                            width: 8.w,
                                          ),
                                          Flexible(
                                            flex: 1,
                                            child: TextFormField(
//                                            keyboardType: TextInputType.phone,
                                              cursorColor: Colors.grey[800],
                                              decoration: InputDecoration(
                                                border: InputBorder.none,
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                      Divider(
                                        height: 1.h,
                                        color: Colors.grey[600],
                                      )
                                    ],
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 20.h,
                              ),
                              Align(
                                alignment: Alignment(0.8, -1),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    NormalButton(
                                      horizontalPadding: 30,
                                      verticalPadding: 10,
                                      text: "kick off",
                                      onPressed: navigateToVerify,
                                      splashColor: Colors.grey[100],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )),
              ]),
        ),
      ),
    );
  }

  void navigateToInBoard() {
    ExtendedNavigator.root.replace(Routes.onboardScreen);
  }

  void navigateToVerify() {
    ExtendedNavigator.root.push(Routes.verifyScreen);
  }
}
