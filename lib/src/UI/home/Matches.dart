import 'package:flutter/material.dart';
import 'package:app/src/widgets/HeaderItemVertical.dart';
import 'package:app/src/widgets/MatchItem.dart';
import 'package:app/src/models/Match.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MatchesScreen extends StatefulWidget {
  final List<Match> matches;
  Color color;
  final TextStyle headerStyle;
  MatchesScreen({this.matches, this.color, this.headerStyle});
  @override
  _MatchesScreenState createState() => _MatchesScreenState();
}

class _MatchesScreenState extends State<MatchesScreen> {
  List<Match> _matches;
  
  @override
  void initState() {
    super.initState();
    _matches = widget.matches;
  }
  
  @override
  Widget build(BuildContext context) {
    List<Widget> items = [];
    _matches.forEach((element) {
      items.add(HeaderItemVertical(name: element.league, style: widget.headerStyle,));
      element.types.forEach((item) {
        items.add(MatchItem(viewType: item,));
      });
    });
    return Container(
      padding: EdgeInsets.only(left: 20.w, right: 20.w, top: 15.h),
      color: widget.color == null ? Colors.grey[50] : widget.color,
      child: ListView(
        children: items,
      ),
    );
  }
}









