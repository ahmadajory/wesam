import 'package:app/src/models/Match.dart';
import 'package:flutter/material.dart';
import 'package:app/util/TextStyles.dart';
import 'package:app/util/Colors.dart';
import 'package:app/src/UI/home/Matches.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeContainerScreen extends StatefulWidget {
  @override
  _HomeContainerScreenState createState() => _HomeContainerScreenState();
}

class _HomeContainerScreenState extends State<HomeContainerScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 4,
        child: Padding(
          padding:  EdgeInsets.only(top: 8.h, left: 0),
          child: Scaffold(
            backgroundColor: Colors.white,
            appBar: TabBar(
              unselectedLabelColor: Colors.black,
              isScrollable: true,
              labelPadding: EdgeInsets.only(left: 25.w, right: 10.w, bottom: 6.h),
              labelStyle: TextStyles.StyleBold14(),
              indicatorColor: AppColors.primary,
              indicatorWeight: 3,
              indicatorSize: TabBarIndicatorSize.tab,
              indicatorPadding: EdgeInsets.only(left: 10.w),
              tabs: <Widget>[


                Text("LIVE",style: TextStyles.StyleNormal16()),
                Text("UPCOMING",style: TextStyles.StyleNormal16()),
                Text("FINISHED",style: TextStyles.StyleNormal16()),
                Text("ALL",style: TextStyles.StyleNormal16()),
              ],
            ),
           body: TabBarView(
             children: [

               MatchesScreen(matches: liveMatches,),
               MatchesScreen(matches: upcomingMatches,),
               MatchesScreen(matches: finishedMatches,),
               MatchesScreen(matches: allMatches,),
             ],
           ),
          ),
        ),

    );
  }
}
