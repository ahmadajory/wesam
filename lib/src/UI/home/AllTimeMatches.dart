import 'package:app/src/models/Match.dart';
import 'package:flutter/material.dart';
import 'package:app/src/widgets/dynamicListView.dart';
import 'package:app/src/widgets/DateItem.dart';
import 'package:app/src/widgets/HeaderItemHorizontal.dart';
import 'package:app/src/UI/home/Matches.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AllTimeMatchesScreen extends StatefulWidget {
  @override
  _AllTimeMatchesUItate createState() => _AllTimeMatchesUItate();
}

class _AllTimeMatchesUItate extends State<AllTimeMatchesScreen> {
  final List<String> leagues = ["All","Champions league", "La Liga", "Premier League", ];
  static final int days = 60;
  final dates = List<DateTime>.generate(days,
          (int index) => index < days / 2
              ? DateTime.now().subtract(Duration(days: 30  - index))
              : DateTime.now().add(Duration(days: index - 30)));
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("All Matches"),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            padding:  EdgeInsets.only(left: 17.w),
            color: Colors.white,
            height: 30.h,
//            child: Text(""),
            child: DynamicListView<String>(data: leagues,
                   itemBuilder: (name, isActive) => HeaderItemHorizontal(name: name, isActive: isActive,),
                  defaultSelectedIndex: 0, direction: Axis.horizontal
                  
             ),
          ),
          SizedBox(height: 12.h,),
          Container(
            color: Colors.white,
            height: 60.h,
            child: DynamicListView<DateTime>(data: dates,
                  itemBuilder: (date, isActive) => DateItem(dateTime: date,isActive: isActive,),
                  defaultSelectedIndex: 30, direction: Axis.horizontal,
                  size: 46.h, position: Position.CENTER,
            ),
          ),

          Flexible(
            flex: 10,
            child: MatchesScreen(matches: allMatches),

          )
        ],
      ),
    );
  }
}






