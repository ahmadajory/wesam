import 'package:app/src/routes/RouterFactory.dart';
import 'package:flutter/material.dart';
import 'package:app/util/Assets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();

    Future.delayed(
      const Duration(seconds: 2),
      () {
        RouterFactory.navigateToOnboard();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      child: Center(
        child: SvgPicture.asset(
          getSVG("logoBall.svg"),
          width: 80.w,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
