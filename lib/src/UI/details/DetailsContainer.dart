import 'package:app/src/models/Match.dart';
import 'package:app/src/models/MatchTypes.dart';
import 'package:app/src/UI/team/Players.dart';
import 'package:app/src/widgets/MatchItem.dart';
import 'package:flutter/material.dart';
import 'package:app/util/TextStyles.dart';
import 'package:app/util/Colors.dart';
import 'package:app/src/UI/home/Matches.dart';
import 'package:app/util/Assets.dart';
import 'package:app/src/UI/team/components/Standings.dart';
class DetailsContainerScreen extends StatefulWidget {
  @override
  _DetailsContainerUItate createState() => _DetailsContainerUItate();
}

class _DetailsContainerUItate extends State<DetailsContainerScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 60, left: 0, right: 0),
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  Text("Champions league"),
                  MatchItem(viewType: MatchesType.LIVE, place: MatchItemPlace.DETAILS,),
                ],
              ),),
            Expanded(
              child: DefaultTabController(
                length: 3,
                child: Scaffold(
                  backgroundColor: Colors.white,
                  appBar: TabBar(
                    unselectedLabelColor: Colors.black,
                    isScrollable: true,
                    labelPadding: EdgeInsets.only(left: 25, right: 10, bottom: 6),
                    labelStyle: TextStyles.StyleBold14(),
                    indicatorColor: AppColors.primary,
                    indicatorWeight: 3,
                    indicatorSize: TabBarIndicatorSize.tab,
                    indicatorPadding: EdgeInsets.only(left: 20),
                    tabs: <Widget>[
                      Text("MATCHES"),
                      Text("STANDINGS"),
                      Text("PLAYERS"),
                    ],
                  ),
                  body: TabBarView(
                    children: [
                      MatchesScreen(matches: allMatchesForTeam, color: Colors.white, headerStyle: TextStyles.StyleMedium20(),),
                      StandingsScreen(),
                      PlayersScreen(),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

//import 'package:app/src/models/Match.dart';
//import 'package:app/src/UI/team/Players.dart';
//import 'package:flutter/material.dart';
//import 'package:app/util/TextStyles.dart';
//import 'package:app/util/Colors.dart';
//import 'package:app/src/UI/home/Matches.dart';
//import 'package:app/util/Assets.dart';
//import 'package:app/src/UI/team/Standings.dart';
//class TeamContainerScreen extends StatefulWidget {
//  @override
//  _TeamContainerUItate createState() => _TeamContainerUItate();
//}
//
//class _TeamContainerUItate extends State<TeamContainerScreen> {
//  @override
//  Widget build(BuildContext context) {
//    return DefaultTabController(
//      length: 3,
//      child: Scaffold(
//          backgroundColor: Colors.white,
//          appBar: AppBar(
//              elevation: 0,
//              backgroundColor: Colors.white,
//              flexibleSpace: Container(height: 200,
//                padding: EdgeInsets.only(top: 20, left: 25),
//                color: Colors.white,
//                child: Row(
//                  crossAxisAlignment: CrossAxisAlignment.center,
//                  children: <Widget>[
//                    Stack(
//                      children: <Widget>[
//                        Container(width: 102, height: 102,
//                          decoration: BoxDecoration(
//                              color: Colors.green[700].withOpacity(0.2),
//                              borderRadius: BorderRadius.circular(51)
//                          ),
//                          child: Center(child: Image.asset(
//                            getImage("realmadrid.png"), width: 60,
//                            height: 60,)),),
//                        Positioned(
//                            right: 0, top: 0,
//                            child: Container(width: 28, height: 28,
//                              decoration: BoxDecoration(
//                                  borderRadius: BorderRadius.circular(14),
//                                  color: AppColors.primary,
//                                  border: Border.all(color: Colors.white,
//                                      width: 2)
//                              ),
//                              child: Center(child: Icon(Icons.star_border,
//                                size: 16,)),
//                            ))
//                      ],
//                    ),
//                    SizedBox(width: 20,),
//                    Wrap(
//                      direction: Axis.vertical,
//                      children: <Widget>[
//                        Text(
//                          "F.C Barcaluna", style: TextStyles.StyleMedium22(),),
//                        Text("Camp Nou",
//                          style: TextStyles.Style12(color: Colors.grey[400]),),
//                      ],
//                    )
//                  ],
//                ),),
//              bottom: TabBar(
//                unselectedLabelColor: Colors.black,
//                isScrollable: true,
//                labelPadding: EdgeInsets.only(left: 25, right: 10, bottom: 6),
//                labelStyle: TextStyles.StyleBold14(),
//                indicatorColor: AppColors.primary,
//                indicatorWeight: 3,
//                indicatorSize: TabBarIndicatorSize.tab,
//                indicatorPadding: EdgeInsets.only(left: 20),
//                tabs: <Widget>[
//                  Text("MATCHES"),
//                  Text("STANDINGS"),
//                  Text("PLAYERS"),
//                ],
//
//              )),
//          body: TabBarView(
//            children: [
//              MatchesScreen(matches: allMatchesForTeam,
//                color: Colors.white,
//                headerStyle: TextStyles.StyleMedium20(),),
//              StandingsScreen(),
//              PlayersScreen(),
//            ],
//          )
//
//      ),
//    );
//  }
//}
