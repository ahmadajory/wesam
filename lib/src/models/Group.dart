class Group{
  final String name;
  final List<int> standingsData;

  Group({this.name, this.standingsData});
}

final groupsStandingsData = [
                              Group(name: "Group A", standingsData: List.generate(4, (index) => index + 1)),
                              Group(name: "Group B", standingsData: List.generate(4, (index) => index + 1)),
                              Group(name: "Group C", standingsData: List.generate(4, (index) => index + 1)),
                              Group(name: "Group D", standingsData: List.generate(4, (index) => index + 1)),
                            ];

