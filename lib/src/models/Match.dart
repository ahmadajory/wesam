import 'package:app/src/models/MatchTypes.dart';

class Match{
  final String league;
  final List<MatchesType> types;

  Match({this.league, this.types});
}

final allMatches = [Match(league: "Champions league", types: [MatchesType.IN_PREDICTED, MatchesType.LIVE]),
                    Match(league: "Champions league", types: [MatchesType.FINISHED, MatchesType.NOT_PREDICTED])];

final allMatchesForTeam = [Match(league: "Upcoming", types: [MatchesType.IN_PREDICTED, MatchesType.LIVE]),
  Match(league: "Finished", types: [MatchesType.FINISHED, MatchesType.NOT_PREDICTED])];


final liveMatches = [Match(league: "Champions league", types: [MatchesType.LIVE, MatchesType.LIVE]),
  Match(league: "Champions league", types: [MatchesType.LIVE, MatchesType.LIVE])];


final upcomingMatches = [Match(league: "Champions league", types: [MatchesType.IN_PREDICTED, MatchesType.NOT_PREDICTED]),
  Match(league: "Champions league", types: [MatchesType.NOT_PREDICTED, MatchesType.IN_PREDICTED])];


final finishedMatches = [Match(league: "Champions league", types: [MatchesType.FINISHED, MatchesType.FINISHED]),
  Match(league: "Champions league", types: [MatchesType.FINISHED, MatchesType.FINISHED])];
