import 'package:app/src/UI/MainContainer.dart';
import 'package:app/src/UI/home/HomeContainer.dart';
import 'package:app/src/UI/onboard/Onboard.dart';
import 'package:app/src/UI/onboard/components/OnboardItem.dart';
import 'package:app/src/UI/signup/Favorite.dart';
import 'package:app/src/UI/signup/Login.dart';
import 'package:app/src/UI/signup/Profile.dart';
import 'package:app/src/UI/signup/Verify.dart';
import 'package:app/src/UI/team/TeamContainer.dart';
import 'package:auto_route/auto_route_annotations.dart';
import 'package:app/src/UI/Splash.dart';

@AdaptiveAutoRouter(routes: <AutoRoute>[
    AdaptiveRoute(page: SplashScreen, initial: true),
    AdaptiveRoute(page: OnboardScreen),
    AdaptiveRoute(page: LoginScreen),
    AdaptiveRoute(page: VerifyScreen),
    AdaptiveRoute(page: ProfileScreen),
    AdaptiveRoute(page: FavoriteScreen),
    AdaptiveRoute(page: MainContainerScreen),
    AdaptiveRoute(page: TeamContainerScreen),
  ]
)
class $CustomRouter{
}