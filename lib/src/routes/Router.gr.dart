// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';

import '../UI/MainContainer.dart';
import '../UI/Splash.dart';
import '../UI/onboard/Onboard.dart';
import '../UI/signup/Favorite.dart';
import '../UI/signup/Login.dart';
import '../UI/signup/Profile.dart';
import '../UI/signup/Verify.dart';
import '../UI/team/TeamContainer.dart';

class Routes {
  static const String splashScreen = '/';
  static const String onboardScreen = '/onboard-screen';
  static const String loginScreen = '/login-screen';
  static const String verifyScreen = '/verify-screen';
  static const String profileScreen = '/profile-screen';
  static const String favoriteScreen = '/favorite-screen';
  static const String mainContainerScreen = '/main-container-screen';
  static const String teamContainerScreen = '/team-container-screen';
  static const all = <String>{
    splashScreen,
    onboardScreen,
    loginScreen,
    verifyScreen,
    profileScreen,
    favoriteScreen,
    mainContainerScreen,
    teamContainerScreen,
  };
}

class CustomRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.splashScreen, page: SplashScreen),
    RouteDef(Routes.onboardScreen, page: OnboardScreen),
    RouteDef(Routes.loginScreen, page: LoginScreen),
    RouteDef(Routes.verifyScreen, page: VerifyScreen),
    RouteDef(Routes.profileScreen, page: ProfileScreen),
    RouteDef(Routes.favoriteScreen, page: FavoriteScreen),
    RouteDef(Routes.mainContainerScreen, page: MainContainerScreen),
    RouteDef(Routes.teamContainerScreen, page: TeamContainerScreen),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    SplashScreen: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => SplashScreen(),
        settings: data,
      );
    },
    OnboardScreen: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => OnboardScreen(),
        settings: data,
      );
    },
    LoginScreen: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => LoginScreen(),
        settings: data,
      );
    },
    VerifyScreen: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => VerifyScreen(),
        settings: data,
      );
    },
    ProfileScreen: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => ProfileScreen(),
        settings: data,
      );
    },
    FavoriteScreen: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => FavoriteScreen(),
        settings: data,
      );
    },
    MainContainerScreen: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => MainContainerScreen(),
        settings: data,
      );
    },
    TeamContainerScreen: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => TeamContainerScreen(),
        settings: data,
      );
    },
  };
}
