import 'package:auto_route/auto_route.dart';

import 'Router.gr.dart';

class RouterFactory{

  static void navigateToOnboard(){
    ExtendedNavigator.root.replace(Routes.onboardScreen);
  }
}